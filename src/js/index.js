export default async function getQuote(cb) {
	const URL = 'https://api.adviceslip.com/advice';

	const response = await fetch(URL);
	const result = await response.json();
	// console.log(result);

	cb(result);
}
